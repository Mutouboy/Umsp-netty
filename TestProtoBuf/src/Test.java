import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import stream.Simple;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class Test {
    public static String KEY_USERID = "userid";
    public static String remotePort = "9982";
    public static int gameID = 201865;
    public static String svcName = "svc-201865-0";
    public static String podName = "";
    public static int userID = (svcName + podName).hashCode();
    public static String localHost = "0.0.0.0";

    public static Simple.Package.Frame PushToHotelBuild(int cmd, ByteString msg) {
        int reverse = 0;
        Simple.Package.Frame.Builder frame = Simple.Package.Frame.newBuilder();
        frame.setCmdId(cmd);
        frame.setType(Simple.Package.FrameType.PushMessage);
        frame.setVersion(2);
        frame.setUserId(userID);
        frame.setMessage(msg);
        frame.setReserved(reverse++);
        Simple.Package.Frame build = frame.build();
        byte[] bytes = build.toByteArray();
        for (int i = 0; i < bytes.length; i++) {
            String s = Integer.toHexString(Byte.toUnsignedInt(bytes[i]));
            s = (s.length() < 2 ? "0" : "") + s;
            if (i % 8 == 0) {
                System.out.print("  ");
            }
            if (i % 16 == 0) {
                System.out.println("\r\n");
            }
            System.out.print(" " + s);
        }

        return build;
    }

    public static void main(String[] args) throws UnsupportedEncodingException, InvalidProtocolBufferException {

        String encpd = "08:01:10:02:18:e1:0b:20:d4:87:df:d4:02:32:94:02:08:03:10:d6:ac:0c:18:b2:a0:80:e0:a8:e5:9b:d6:17:2a:81:02:7b:22:74:79:70:65:22:3a:22:6d:6f:76:65:22:2c:22:64:61:74:61:22:3a:5b:7b:22:73:74:61:74:75:73:22:3a:31:2c:22:73:63:6f:72:65:22:3a:38:30:2c:22:73:69:7a:65:22:3a:32:30:2c:22:78:22:3a:31:32:37:31:2c:22:79:22:3a:31:32:34:34:2c:22:73:70:65:65:64:22:3a:35:2c:22:75:73:65:72:49:44:22:3a:31:35:38:33:38:36:34:7d:2c:7b:22:73:74:61:74:75:73:22:3a:31:2c:22:73:63:6f:72:65:22:3a:32:30:2c:22:73:69:7a:65:22:3a:32:30:2c:22:78:22:3a:31:31:31:39:2c:22:79:22:3a:38:34:35:2c:22:73:70:65:65:64:22:3a:35:2c:22:75:73:65:72:49:44:22:3a:31:35:38:33:39:34:34:7d:2c:7b:22:73:74:61:74:75:73:22:3a:31:2c:22:73:63:6f:72:65:22:3a:30:2c:22:73:69:7a:65:22:3a:31:2c:22:78:22:3a:31:36:33:36:2c:22:79:22:3a:37:39:38:2c:22:73:70:65:65:64:22:3a:35:2c:22:75:73:65:72:49:44:22:3a:31:35:38:33:38:39:30:7d:5d:7d";
//        String encpd0 =   "00:00:5f:00:00:00:00:00:01:00:00:00:00:5a:08:80:08:10:01:18:e0:0b:28:b5:31:32:4d:08:c8:d6:60:10:d6:ac:0c:18:b2:a0:80:e0:a8:e5:9b:d6:17:20:25:32:37:7b:22:74:79:70:65:22:3a:22:69:6e:70:75:74:22:2c:22:64:61:74:61:22:3a:7b:22:6c:22:3a:30:2c:22:72:22:3a:31:2c:22:75:22:3a:31:2c:22:64:22:3a:30:2c:22:70:22:3a:30:7d:7d";
//        String encpd =   "80:08:10:01:18:e0:0b:28:b5:31:32:4d:08:c8:d6:60:10:d6:ac:0c:18:b2:a0:80:e0:a8:e5:9b:d6:17:20:25:32:37:7b:22:74:79:70:65:22:3a:22:69:6e:70:75:74:22:2c:22:64:61:74:61:22:3a:7b:22:6c:22:3a:30:2c:22:72:22:3a:31:2c:22:75:22:3a:31:2c:22:64:22:3a:30:2c:22:70:22:3a:30:7d:7d";
       String encpd30 = "00:00:15:00:00:00:00:00:01:00:00:00:00:10:08:80:08:10:01:18:e0:0b:28:e0:15:32:03:10:c8:01";
       String encpd17 = "00:00:08:06:01:00:00:00:00:02:04:10:10:09:0e:07:07";
        String[] split = encpd.split(":");


        byte[] bytes1 = "Stream".getBytes();
        System.out.print("steam:");
        for (int i = 0; i < bytes1.length; i++) {
        System.out.print(Integer.toHexString((bytes1[i]&0xff))+":");
        }
        System.out.println("steam:====");

        byte[] bytes = new byte[split.length];
        for (int i = 0; i < split.length; i++) {
            bytes[i]  = Integer.valueOf(split[i], 16).byteValue();;
            System.out.print((bytes[i]&0xff)+":");
        }
        System.out.println("==============");
        Simple.Package.Frame frame = Simple.Package.Frame.parseFrom(bytes);
        System.out.println(frame.getCmdId());

        byte[] msg = "{\"type\":\"move\",\"data\":[{\"status\":1,\"score\":0,\"size\":20,\"x\":2106,\"y\":292,\"speed\":5,\"userID\":1585274}]}".getBytes("UTF-8");
        long roomID = 1705926743983919121L;
        int[] userIDs = {1585274};
        Gshotel.PushToHotelMsg.Builder builder = Gshotel.PushToHotelMsg.newBuilder();
        builder.setCpProto(ByteString.copyFrom(msg));
        builder.setPushType(Gshotel.PushMsgType.UserTypeAll);
        builder.setGameID(gameID);
        builder.setRoomID(roomID);
        if (userIDs != null) {
            for (int i = 0; i < userIDs.length; i++) {
                builder.addDstUids(userIDs[i]);
                builder.setPushType(Gshotel.PushMsgType.UserTypeSpecific);
            }
        }
//        logger.debug("room.channel:" + room.channel);
        try {
            PushToHotelBuild(Gshotel.HotelGsCmdID.HotelPushCMDID_VALUE,
                    builder.build().toByteString());
        } catch (Exception e) {
            e.printStackTrace();

        }


    }

}
