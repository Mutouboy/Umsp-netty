//'Umsp' is a java object that it be inject to javascript scope when init javascript engine
var onUserSendMsg = function (msg, room) {
    console.info("onUserSendMsg:" + msg.payload2String() + " from userID:" + msg.userID);

    //
    // {"msgType":"input","data":0}
    var data = JSON.parse(msg.payload2String());
    switch (data.msgType) {
        case 'input':
            var s1 = {msgType: 'move', data: "me"};
            var s = {msgType: 'move', data: "other"};
            Umsp.broadcastOther(Umsp.buildStringMsg(JSON.stringify(s)), room, msg.userID);
            Umsp.broadcastMe(Umsp.buildStringMsg(JSON.stringify(s1)), room, msg.userID);
            break;
        default:
            Umsp.broadcast(msg, room);
            break;
    }
//    print("room.roomID : " + room.roomID);

};