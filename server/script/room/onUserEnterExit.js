if(!this.roomMap){
    this.roomMap = {};
    console.info("init room map");
}

var onUserExit = function (user, room) {
    if (roomMap[room.roomID] != undefined && room.roomUserList.size() == 0) {
        var loop = roomMap[room.roomID].loop;
        clearInterval(loop);
        roomMap[room.roomID].loop = null;
        roomMap[room.roomID] = undefined;
        console.info("[INFO] room is delete " + room.roomID);
    }

};
var onUserEnter = function (user, room) {
    if (roomMap[room.roomID] == undefined) {
        roomMap[room.roomID] = room;
        initRoomLoop(room);
    }
    var userMap = room.roomUserList;

    for each(var e in userMap.keySet())
    {
        console.info("[INFO] e " + e + " :" + userMap.get(e));
    }

};


var initRoomLoop = function (room) {
    var x =0;
    room.loop = setInterval(function () {
        x++;
        var s1 = {msgType: 'move', data: x};
        Umsp.broadcast(Umsp.buildStringMsg(JSON.stringify(s1)), room);
        console.info("[INFO]  room loop ,roomID: " + room.roomID);
    }, 1000);
};
