### 部署
#### 软件安装
    1. 安装JDK 1.8+ 
    2. 安装Redis 3.2+
    3. 安装服务端IDE(Intellij IDEA Community 2017.2+/eclipse3.5+)
    4. 安装客户端IDE(Visual Studio Community 2017 & Unity5.x/Cocos3.10+)
    5. 辅助工具(RedisDestopManager WireShark NotePad++)
#### 软件配置
    1. 修改Redis配置文件,启用Auth密码(必须),示例: 
        ################################## SECURITY ###################################
        
        # Require clients to issue AUTH <PASSWORD> before processing any other
        # commands.  This might be useful in environments in which you do not trust
        # others with access to the host running redis-server.
        #
        # This should stay commented out for backward compatibility and because most
        # people do not need auth (e.g. they run their own servers).
        #
        # Warning: since Redis is pretty fast an outside user can try up to
        # 150k passwords per second against a good box. This means that you should
        # use a very strong password otherwise it will be very easy to break.
        #
        requirepass redismima
         
        
    2. 修改软件数据库配置(必须):
        修改Reids的链接信息和密码 
        文件:gl/java/umsp/db/redis/RedisUtil.java.
        示例:
            //Redis服务器IP
            public static String ADDR = "127.0.0.1";
            //private static String ADDR = "erdange.com";
        
            //Redis的端口号
            public static int PORT = 6379;
        
            //访问密码
            public static String AUTH = "redismima";
        
            //可用连接实例的最大数目，默认值为8；
            //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
            public static int MAX_ACTIVE = 1024;
        
            //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
            public static int MAX_IDLE = 200;
        
            //等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
            public static int MAX_WAIT = 5000;
        
            public static int TIMEOUT = 10000;
             
    3. 修改软件网络监听端口配置(可选)
        1)修改GateWay默认端口(有默认为8888).
          文件:gl/java/umsp/gateway/GateWayServiceConfig.java
        2)修改MatchService支持的gameID(有默认0).
          文件:gl/java/umsp/match/MatchServiceConfig.java
        3)修改RoomService支持的GameID/ProxyServiceIP/RoomIndex等信息(有默认配置)
          文件:gl/java/umsp/room/RoomServiceConfig.
          
#### 软件启动
    使用Java的jar命令调用各个服务的jar
    启动服务端:
    1. 启动GateWay (gl/java/umsp/gateway/GateWayService.java)
    2. 启动MatchService(gl/java/umsp/match/MatchService.java)
    3. 启动RoomService(gl/java/umsp/room/RoomService.java)
    ps: 开发时可以使用IDE在各个文件启动Main函数
    启动客户端:
    1. 启动UmspClient (gl/java/umsp/client/UmspClient.java)