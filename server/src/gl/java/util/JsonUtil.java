package gl.java.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;
import com.jsoniter.output.EncodingMode;
import com.jsoniter.output.JsonStream;
import com.jsoniter.spi.DecodingMode;

import java.lang.reflect.Type;

public class JsonUtil {
    static {
        JsonIterator.setMode(DecodingMode.DYNAMIC_MODE_AND_MATCH_FIELD_WITH_HASH);
        JsonStream.setMode(EncodingMode.DYNAMIC_MODE);
    }
    public static <T> T fromJson(String json ,Class<T> clazz){
        return new Gson().fromJson(json, clazz);
    }
    public static <T> T fromJson2(String json ,Class<T> clazz){
        return JsonIterator.deserialize(json,clazz);
    }
    public static <T> T fromJson(String json ,Type clazz){
        return new Gson().fromJson(json, clazz);
    }

    public static  String toString(Object object) {
        return new Gson().toJson(object);
    }

    public static  String toString2(Object object) {
        return JsonStream.serialize(object);
    }
}
