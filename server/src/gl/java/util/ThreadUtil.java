package gl.java.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadUtil {
    public static ExecutorService createSingleBlockThread(int maxTaskSize) {
        return new ThreadPoolExecutor(
                1,
                1,
                0,
                TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<Runnable>(maxTaskSize),
                new ThreadPoolExecutor.AbortPolicy());
    }

    public static ExecutorService createSingleBlockThread() {
        return new ThreadPoolExecutor(
                1,
                1,
                0,
                TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<Runnable>(1024),
                new ThreadPoolExecutor.AbortPolicy());
    }
}
