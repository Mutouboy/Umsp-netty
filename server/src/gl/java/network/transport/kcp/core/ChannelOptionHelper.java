package gl.java.network.transport.kcp.core;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.UkcpServerBootstrap;

/**

 */
public class ChannelOptionHelper {
    /**
     *
     * 普通模式：`ikcp_nodelay(kcp, 0, 40, 0, 0); 极速模式： ikcp_nodelay(kcp, 1, 10, 2, 1);
     * @param b
     * @param nodelay 是否启用nodelay模式，0不启用；1启用
     * @param interval 协议内部工作的 interval，单位毫秒
     * @param fastResend  快速重传模式，默认0关闭，可以设置2（2次ACK跨越将会直接重传）
     * @param nocwnd nc ：是否关闭流控，默认是0代表不关闭，1代表关闭
     * @return
     */
    public static Bootstrap nodelay(Bootstrap b, boolean nodelay, int interval, int fastResend, boolean nocwnd) {
        b.option(UkcpChannelOption.UKCP_NODELAY, nodelay)
                .option(UkcpChannelOption.UKCP_INTERVAL, interval)
                .option(UkcpChannelOption.UKCP_FAST_RESEND, fastResend)
                .option(UkcpChannelOption.UKCP_NOCWND, nocwnd);
        return b;
    }

    /**
     *
     * 普通模式：`ikcp_nodelay(kcp, 0, 40, 0, 0); 极速模式： ikcp_nodelay(kcp, 1, 10, 2, 1);
     * @param b
     * @param nodelay 是否启用nodelay模式，0不启用；1启用
     * @param interval 协议内部工作的 interval，单位毫秒
     * @param fastResend  快速重传模式，默认0关闭，可以设置2（2次ACK跨越将会直接重传）
     * @param nocwnd nc ：是否关闭流控，默认是0代表不关闭，1代表关闭
     * @return
     */
    public static UkcpServerBootstrap nodelay(UkcpServerBootstrap b, boolean nodelay, int interval, int fastResend,
                                              boolean nocwnd) {
        b.childOption(UkcpChannelOption.UKCP_NODELAY, nodelay)
                .childOption(UkcpChannelOption.UKCP_INTERVAL, interval)
                .childOption(UkcpChannelOption.UKCP_FAST_RESEND, fastResend)
                .childOption(UkcpChannelOption.UKCP_NOCWND, nocwnd);
        return b;
    }

}
