package gl.java.network.transport.kcp.example.rtt;

import gl.java.network.transport.kcp.core.ChannelOptionHelper;
import gl.java.network.transport.kcp.core.UkcpChannel;
import gl.java.network.transport.kcp.core.UkcpChannelOption;
import gl.java.network.transport.kcp.core.UkcpServerChannel;
import gl.java.umsp.UmspAutoHeartBeatHandler;
import io.netty.bootstrap.UkcpServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Measures RTT(Round-trip time) for KCP.
 * <p>
 * Receives a message from client and sends a response.
 *

 */
public class KcpRttServer {

    static final int CONV = Integer.parseInt(System.getProperty("conv", "10"));
    static final int PORT = Integer.parseInt(System.getProperty("port", "8009"));
    static Logger log = LoggerFactory.getLogger(KcpRttServer.class);
    public static void main(String[] args) throws Exception {
        // Configure the server.
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            UkcpServerBootstrap b = new UkcpServerBootstrap();
            b.group(group)
                    .channel(UkcpServerChannel.class)
                    .childHandler(new ChannelInitializer<UkcpChannel>() {
                        @Override
                        public void initChannel(UkcpChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(UmspAutoHeartBeatHandler.newTimeOutCloseHandler(p));
                            p.addLast(new KcpRttServerHandler());
                        }
                    });
            ChannelOptionHelper.nodelay(b, true, 20, 2, true)
                    .childOption(UkcpChannelOption.UKCP_MTU, 512);

            // Start the server.
            ChannelFuture f = b.bind(PORT).sync();
            f.addListener(new GenericFutureListener<Future<? super Void>>() {
                @Override
                public void operationComplete(Future<? super Void> future) throws Exception {
                    log.info("listen at port:"+PORT);
                }
            });
            // Wait until the server socket is closed.
            f.channel().closeFuture().sync();
        } finally {
            // Shut down all event loops to terminate all threads.
            group.shutdownGracefully();
        }
    }

}
