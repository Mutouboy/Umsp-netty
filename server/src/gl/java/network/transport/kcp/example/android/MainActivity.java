//package gl.java.network.transport.kcp.example.android;
//
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import gl.java.network.transport.kcp.example.rtt.KcpRttClient;
//import gl.java.network.transport.kcp.example.rtt.TcpRttClient;
//
//public class MainActivity extends Activity {
//    private final String TAG = "MainActivity";
//
//    static {
//        System.setProperty("host", "192.168.8.102");
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Button kcp = new Button(this);
//        kcp.setText("kcp");
//        kcp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new Thread() {
//                    @Override
//                    public void run() {
//                        try {
//                            KcpRttClient.main(null);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }.start();
//            }
//        });
//        Button tcp = new Button(this);
//        tcp.setText("tcp");
//        tcp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new Thread() {
//                    @Override
//                    public void run() {
//                        try {
//                            TcpRttClient.main(null);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }.start();
//            }
//        });
//
//        LinearLayout ll = new LinearLayout(this);
//        ll.addView(kcp,200,100);
//        ll.addView(tcp,200,100);
////        setContentView(nativeAndroid.getRootFrameLayout());
//        setContentView(ll);
//
//    }
//}
