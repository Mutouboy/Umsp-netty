package gl.java.network.transport.kcp.umsp;

import gl.java.network.transport.kcp.core.UkcpChannel;
import gl.java.umsp.Umsp;
import gl.java.umsp.UmspConnectionHandler;
import gl.java.umsp.UmspHeader;
import gl.java.umsp.event.Event;
import gl.java.umsp.router.ChannelRouter;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@ChannelHandler.Sharable
public class UmspKcpConnectionHandler extends UmspConnectionHandler {
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        log.info("channelActive" + ctx.channel().remoteAddress());
        log.info("conv:"+((UkcpChannel)ctx.channel()).conv());
        //TODO 将服务端分类的conv发给客户端. 可以实现wifi/4G切换掉线无感知重连((UkcpChannel)ctx.channel()).conv(clientID)
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        log.info("channelInactive" + ctx.channel().remoteAddress());
    }

}
