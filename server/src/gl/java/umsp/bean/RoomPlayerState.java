package gl.java.umsp.bean;

/**
 * 房间内玩家状态
 *
 * @author geliang
 */
public interface RoomPlayerState {
    /**
     * 在房间外
     */
    public final static int IDLE = 0;
    /**
     * 在房间内并且准备就绪
     */
    public final static int READY = 1;
    /**
     * 在房间内没有准备
     */
    public final static int UNREADY = READY<<2;
    /**
     * 在房间内游戏结算完成
     */
    public final static int OVER = READY<<3;
    /**
     * 在房间内游戏中
     */
    public final static int PLAY = READY<<4;
    /**
     * 在房间内离线中(失去网络连接)
     */
    public final static int OFFLINE = READY<<5;
}
