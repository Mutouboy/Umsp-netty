package gl.java.umsp.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Match extends JsonBean {
    int matchType;
    /**
     * 房间最大人数,默认为2
     */
    int maxUserCount = 2;
    /**
     * 房间名(相等值匹配)
     */
    String roomName;

    /**
     * 房间标签,房间临时存储数据,可增删改查,有长度限制
     */
    String roomTag;

    /**
     * 滚雪球匹配值(相近值匹配)
     */
    int rollValue;
    /**
     * <p>滚雪球匹配扩散范围(近值匹配)</p>
     * 比如rollValue为10,rollRang为5,roll一次的匹配范围5~15,
     * roll第二次的匹配范围0~20
     */
    int rollRang;

    int matchServiceIndex;
    User wantToMatchUser;

    public Match(int matchType) {
        super();
        this.matchType = matchType;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
