package gl.java.umsp.bean;

import lombok.Data;

@Data
public class ErrMsg {
    int errCode;
    String errString;
    int userID;
}
