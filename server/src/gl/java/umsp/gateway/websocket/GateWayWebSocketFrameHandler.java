package gl.java.umsp.gateway.websocket;

import gl.java.umsp.UmspConfig;
import gl.java.umsp.UmspHeader;
import gl.java.umsp.gateway.GateWayMessageDispatcher;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GateWayWebSocketFrameHandler extends
        SimpleChannelInboundHandler<BinaryWebSocketFrame> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, BinaryWebSocketFrame frame) throws Exception {
        Channel incoming = ctx.channel();
        ByteBuf buf = frame.content();
        ctx.channel().attr(UmspConfig.WebSocketBin).set(1);
        GateWayMessageDispatcher.getInstance().channelRead0(ctx, UmspHeader.readMessage(buf));
    }

//    public static ChannelGroup channels = new DefaultChannelGroup(
//            GlobalEventExecutor.INSTANCE);
}