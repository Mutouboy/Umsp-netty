package gl.java.umsp.event;

public interface IEventSubscriber {
    public void onMessage(String channel, String key);
}
