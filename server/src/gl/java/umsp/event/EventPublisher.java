package gl.java.umsp.event;

import gl.java.mq.Producer;
import lombok.extern.slf4j.Slf4j;

/**
 * Event事件发布者
 */
@Slf4j
public class EventPublisher {
    volatile static Producer producer;
    public static void pub(String channel, String value) {
        if (producer == null) {
            synchronized (EventSubscriber.class) {
                producer = new Producer(EventServerConfig.ServerUrl, EventServerConfig.ServerPort);
                producer.start();
            }
        }
        producer.publish(channel, value);
    }
}
