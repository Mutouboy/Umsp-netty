package gl.java.umsp.room;

import gl.java.umsp.Umsp;
import gl.java.umsp.UmspHeader;
import gl.java.umsp.bean.Room;
import gl.java.umsp.bean.User;
import gl.java.umsp.room.framework.RoomServerFramework;
import gl.java.umsp.router.ChannelRouter;
import gl.java.util.PropertiesUtil;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Set;

@Slf4j
public class RoomService extends RoomServiceBase {
    static {
        PropertiesUtil.loadProperties("config.properties");
    }

    public static RoomService INSTANCE = new RoomService();

    RoomService() {
        super();
        framework = new RoomServerFramework(this);
    }

    public static void main(String[] args) throws FileNotFoundException, ScriptException {
        RoomServiceConfig defaultConfig = RoomServiceConfig.getDefaultConfig();
//        if (args != null && args.length > 0) {
//            ScriptEngine.loadScriptFileByPath(args[0]);
//            String getRoomServerConfig = ScriptEngine.invokeFunction("getRoomServerConfig").toString();
//            if (TextUtil.isNotEmpty(getRoomServerConfig)) {
//                defaultConfig = RoomServiceConfig.getConfigByJsonString(getRoomServerConfig);
//                log.info("load room server config from javaScript:" + defaultConfig);
//            }
//        } else {
//            ScriptEngine.loadScriptFileByUrl("http://localhost:4800/script/script.js");
//            String getRoomServerConfig = ScriptEngine.invokeFunction("getRoomServerConfig").toString();
//            if (TextUtil.isNotEmpty(getRoomServerConfig)) {
//                defaultConfig = RoomServiceConfig.getConfigByJsonString(getRoomServerConfig);
//                log.info("load room server config from javaScript:" + defaultConfig);
//            }
//        }
//
//        ScriptEngine.getEngine().put("Umsp", RoomService.INSTANCE);
        //启动房间服务
        RoomService.INSTANCE.start(defaultConfig);
    }


    public void broadcast(UmspHeader msg, Room room) {
        Map<Integer, User> hashmap = room.getRoomUserList();
        Set<Integer> keys = hashmap.keySet();
        for (Integer key : keys) {
            log.debug(" SendMsg To: " + key);
            int userID = hashmap.get(key).userID;
            msg.userID = userID;
            Umsp.returnMsg(msg, ChannelRouter.getInstance().getChannel(userID));
        }
    }

    public void broadcastOther(UmspHeader msg, Room room, int... userIDs) {
        Map<Integer, User> hashmap = room.getRoomUserList();
        Set<Integer> keys = hashmap.keySet();
        for (int i = 0; i < userIDs.length; i++) {
            for (Integer key : keys) {
                if (userIDs[i] != key) {
                    int userID = hashmap.get(key).userID;
                    msg.userID = userID;
                    Umsp.returnMsg(msg, ChannelRouter.getInstance().getChannel(userID));
                }
            }
        }
        log.debug(" broadcastOther To: " + userIDs);
    }

    public void broadcastMe(UmspHeader msg, Room room, int... userIDs) {
        Map<Integer, User> hashmap = room.getRoomUserList();
        Set<Integer> keys = hashmap.keySet();
        for (int i = 0; i < userIDs.length; i++) {
            for (Integer key : keys) {
                if (userIDs[i] == key) {
                    int userID = hashmap.get(key).userID;
                    msg.userID = userID;
                    Umsp.returnMsg(msg, ChannelRouter.getInstance().getChannel(userID));
                }
            }
        }
        log.debug(" broadcastOther To: " + userIDs);
    }

    public void broadcast(String msg, Room room) {
        Map<Integer, User> hashmap = room.getRoomUserList();
        Set<Integer> keys = hashmap.keySet();
        for (Integer key : keys) {
            log.debug(" SendMsg To: " + key);
            int userID = hashmap.get(key).userID;
            Umsp.returnMsg(buildStringMsg(msg), ChannelRouter.getInstance().getChannel(userID));
        }
    }

    public UmspHeader buildStringMsg(String payload) {
        return new UmspHeader(Umsp.CMD_MSG_RSP, payload);
    }


}
