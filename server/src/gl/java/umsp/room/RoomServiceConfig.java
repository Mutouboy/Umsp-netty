package gl.java.umsp.room;

import com.google.gson.Gson;
import gl.java.umsp.IDistributeService;
import gl.java.umsp.UmspConfig;

/**
 * 服务器配置信息,如区服务信息.
 */
public class RoomServiceConfig extends IDistributeService.Config {
    public static RoomServiceConfig getDefaultConfig() {
        return new RoomServiceConfig();
    }

    /**
     * 房间服务器所属的游戏ID
     */
    public int gameID;
    public int serverIndex = 0;
    public String IP = "127.0.0.1";
    public int port = UmspConfig.DEFAULT_ROOM_PORT;
    public int portWebSocket = UmspConfig.DEFAULT_ROOM_PORT_WEBSOCKET;
    public int portUdp = UmspConfig.DEFAULT_ROOM_PORT_UDP;
    public String desKey = "des-room";


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static RoomServiceConfig fromJson(String json) {
        return new Gson().fromJson(json, RoomServiceConfig.class);
    }

    public static RoomServiceConfig getConfigByJsonString(String json) {
        return new Gson().fromJson(json, RoomServiceConfig.class);
    }
}
