package gl.java.umsp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldPrepender;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.List;

@Slf4j
public class UmspMessagePacker extends LengthFieldPrepender {

    public UmspMessagePacker() {
        super(ByteOrder.LITTLE_ENDIAN, 4, 0, true);
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out)
            throws Exception {
//        if (ctx.channel().attr(UmspConfig.WebSocketBin).get()==0){
//            System.err.println(" bin encode~!");
        super.encode(ctx, msg, out);
//        }else{
//            System.err.println(" websocket encode~!");
//        }
    }

}
