package gl.java.umsp.protocol;

import gl.java.umsp.Umsp;
import gl.java.umsp.UmspHeader;
import io.netty.channel.ChannelHandlerContext;

public class GateWayHandleHeartBeat implements IUmspHandler {
    @Override
    public boolean handle(ChannelHandlerContext ctx, UmspHeader msg) {
        if (Umsp.CMD_HEARTBEAT == msg.cmd){
            Umsp.returnHeart(ctx.channel());
            return true;
        }
        return false;
    }
}
