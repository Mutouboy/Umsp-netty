package gl.java.umsp.match;

import gl.java.umsp.bean.MatchResult;
import gl.java.umsp.event.Event;
import gl.java.umsp.event.EventPublisher;
import gl.java.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MatchUserManager implements IMatchedCallBack {
    public static MatchUserManager INSTANCE = new MatchUserManager();
    public static final String KEY_REDIS = "ToMatchUserList_";

    private MatchUserManager() {
    }

    @Override
    public void onMatched(MatchResult result) {
        String message = JsonUtil.toString(result);
        if (result.isSuccess) {
            log.info("   match success:" + message);
        } else {
            log.warn("   match fail:" + message);
        }
        EventPublisher.pub(Event.CHANNEL_MATCH_EVENT_RESULT, message);
    }

}
