package gl.java.game;


public class NetEvent<Channel,NetDataPacket> {
    public NetEvent(Channel channelHandlerContext, NetDataPacket event) {
        this.context = channelHandlerContext;
        this.frame = event;
    }
    public NetDataPacket frame;
    public Channel context;

}
