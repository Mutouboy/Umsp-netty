package com.demo.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import org.apache.log4j.Logger;

public class CrossOriginInterceptor implements Interceptor {
	private static final Logger LOG = Logger.getLogger(CrossOriginInterceptor.class);

	public void intercept(Invocation inv) {
        inv.getController().getResponse().addHeader("Access-Control-Allow-Origin","*");
	}

}