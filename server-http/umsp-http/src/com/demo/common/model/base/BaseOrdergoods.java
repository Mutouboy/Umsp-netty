package com.demo.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseOrdergoods<M extends BaseOrdergoods<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setForderid(java.lang.Integer forderid) {
		set("forderid", forderid);
	}

	public java.lang.Integer getForderid() {
		return get("forderid");
	}

	public void setFgoodsid(java.lang.Integer fgoodsid) {
		set("fgoodsid", fgoodsid);
	}

	public java.lang.Integer getFgoodsid() {
		return get("fgoodsid");
	}

}
