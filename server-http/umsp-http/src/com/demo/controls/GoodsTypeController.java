package com.demo.controls;

import java.util.List;

import com.demo.common.model.Goodstype;

/**
 * IndexController
 */
public class GoodsTypeController extends BaseController {
	public void index() {
		List<Goodstype> result = Goodstype.dao.find("select * from goodstype where showlevel = 0");
		renderJson(result);
	}
	public void recommend() {
		List<Goodstype> result = Goodstype.dao.find("select * from goodstype where showlevel = 1");
		renderJson(result);
	}
	public void calendar() {
		List<Goodstype> result = Goodstype.dao.find("select * from goodstype where showlevel = 2");
		renderJson(result);
	}
}