package com.demo.controls;

import java.util.List;

import com.demo.common.model.Coupon;

/**
 * IndexController
 */
public class CouponController extends BaseController {
	public void index() {
		int userid;
		if ((userid = checkToken()) < 0) {
			return;
		}

		List<Coupon> users = Coupon.dao.find("select * from coupon where fuserid = ?",userid);
		renderJson(users);
	}
}