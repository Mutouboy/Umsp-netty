package gl.java.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.chanceit.framework.utils.encode.Des;

public class StringUtils {
	public final static int BOM_UTF32_BE = 0x0000FEFF;// UTF-32, big-endian
	public final static int BOMUTF32_LE = 0xFFFE0000;// UTF-32,little-endian
	public final static int BOM_UTF16_BE = 0xFEFF;// UTF-16, big-endian
	public final static int BOM_UTF16_LE = 0xFFFE;// UTF-16, little-endian
	public final static int BOM_UTF8 = 0xEFBBBF;// UTF-8

	public static boolean isEmpty(Object s) {
		return s == null || s.toString().length() <= 0;
	}

	public static String desEncode(Object data) {
		if (data == null) {
			return "";
		}
		return Des.enCrypto(data.toString());
	}

	public static String genToken(String userName, int userID) {
		return MD5.md5(userName + userID);
	}

	public static String enCryPassWord(int userid, String orgpassword) {
		return Des.enCrypto(MD5.md5(userid + orgpassword));
	}

	public static String enCryPayPassWord(int userid, String orgpassword) {
		return Des.enCrypto(MD5.md5(userid + orgpassword));
	}

	/**
	 * get char set from file
	 * 
	 * @deprecated only support with Bom file
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static String getCharset(String fileName) throws IOException {

		BufferedInputStream bin = new BufferedInputStream(new FileInputStream(
				fileName));
		int p = (bin.read() << 8) + bin.read();
		String code = null;
		switch (p) {
		case 0xefbb:
			code = "UTF-8";
			break;
		case 0xfffe:
			code = "Unicode";
			break;
		case 0xfeff:
			code = "UTF-16BE";
			break;
		default:
			code = "";
		}
		bin.close();
		return code;
	}

	public final static BufferedReader openStringFile(String filePath)
			throws IOException {
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(
				filePath));
		skipBom(bis);
		return new BufferedReader(new InputStreamReader(bis));
	}

	/**
	 * 解析bom头，并去掉bom头
	 * 
	 * @deprecated 仅仅测试过UTF-8bom头
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public final static int skipBom(InputStream in) throws IOException {
		byte start2[] = new byte[2];
		in.mark(4);
		int readLen = in.read(start2);
		if (readLen != 2) {
			in.reset();
			return 0;
		}
		if ((start2[0] << 8 & 0xff | start2[1] & 0xff) == BOM_UTF16_BE) {
			return 2;
		}
		if ((start2[0] << 8 | start2[1]) == BOM_UTF16_LE) {
			return 2;
		}
		if ((start2[0] & 0xff) == (BOM_UTF8 >> 16)
				&& (start2[1] & 0xff) == (BOM_UTF8 >> 8 & 0x000000ff)) {
			int byte3;
			if ((byte3 = in.read()) < 0) {
				in.reset();
				return 0;
			}
			if ((byte3 & 0xff) == (BOM_UTF8 & 0x000000ff)) {
				return 3;
			}
		}
		if (start2[0] != 0 && start2[0] != 0) {
			in.reset();
			return 0;
		}

		byte start4[] = new byte[2];
		if (in.read(start4) != 2) {
			in.reset();
			return 0;
		}
		if ((start4[0] << 8 | start4[1]) == BOM_UTF32_BE) {
			return 4;
		}
		return 0;
	}
}
