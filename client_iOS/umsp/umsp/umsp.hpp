/*
 *  umsp.hpp
 *  umsp
 *
 *  Created by 杨雪芹 on 16/8/15.
 *  Copyright © 2016年 geliang. All rights reserved.
 *
 */

#ifndef umsp_
#define umsp_

/* The classes below are exported */
#pragma GCC visibility push(default)

class umsp
{
	public:
		void HelloWorld(const char *);
};

#pragma GCC visibility pop
#endif
