﻿using BestHTTP.Examples;
using UnityEngine;
using System;
using BestHTTP.WebSocket;
using Matchvs;
using Google.Protobuf.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Demo:MonoBehaviour{
    private LinkedList<string> Text = new LinkedList<string>();

    public void startMatch(){
        Response.loginResponse loginResponse = (LoginRsp rsp)=>{
            Log.i("loginResponse {0}",rsp.Status);
            appendLog("loginResponse....  Status:{0}\n",rsp.Status);

            Engine.getInstance().joinRandomRoom(2," from unity,userID: "+Config.userID);
        };
        Response.errorResponse errorResponse = (int errCode,string errMsg)=>{
            Log.i("errorResponse:{0}({1})",errCode,errMsg);
            appendLog("errorResponse..   {0}:{1}.\n",errCode,errMsg);
        };
        Response.initResponse initResponse = (int errCode)=>{
            Log.i("initResponse{0}",errCode);
            appendLog("initResponse{0}",errCode);
            Engine.getInstance().login(Config.userID,Config.token);
        };
        Response.joinRoomResponse joinRoomResponse = (int status,List<PlayerInfo> roomUserInfoList,RoomInfo roomInfo)=>{
            Log.i("joinRoomResponse{0}",status);
            appendLog("joinRoomResponse: roomInfo:{0},userList.size:{1}",roomInfo,roomUserInfoList.Count);
            roomUserInfoList.ForEach((player)=>{
                appendLog("          =======  user:{0}({1})",player.UserID,player.UserProfile);
            });
        };
        Response.joinRoomNotify joinRoomNotify = (PlayerInfo roomUserInfo)=>{
            Log.i("joinRoomNotify{0}",roomUserInfo.UserID);
            appendLog("joinRoomNotify{0}",roomUserInfo.UserID);
        };

        Response.sendEventResponse sendEventResponse = (uint r)=>{
            Log.i("sendEventResponse{0}",r);
            appendLog("sendEventResponse{0}",r);
        };
        Response.sendEventNotify sendEventNotify = (uint u,byte[] data)=>{
            Log.i("sendEventNotify{0} len.{1}",u,data.Length);
            appendLog("sendEventNotify,userid,{0}:{1}",u,StringUtil.fromUtf8Array(data));
        };

        Response.leaveRoomResponse leaveRoomResponse = (LeaveRoomRsp u)=>{
            Log.i("LeaveRoomRsp{0}",u);
            appendLog("sendEventNotify{0}",u);
        };

        Response.leaveRoomNotify leaveRoomNotify = (NoticeLeave u)=>{
            Log.i("leaveRoomNotify{0}",u);
            appendLog("leaveRoomNotify{0},{1}",u.UserID,u);
        };
        Engine.getInstance().listen("loginResponse",loginResponse);
        Engine.getInstance().listen("errorResponse",errorResponse);
        Engine.getInstance().listen("initResponse",initResponse);

        Engine.getInstance().listen("joinRoomResponse",joinRoomResponse);
        Engine.getInstance().listen("joinRoomNotify",joinRoomNotify);
        Engine.getInstance().listen("leaveRoomResponse",leaveRoomResponse);
        Engine.getInstance().listen("leaveRoomNotify",leaveRoomNotify);

        Engine.getInstance().listen("sendEventResponse",sendEventResponse);
        Engine.getInstance().listen("sendEventNotify",sendEventNotify);


        Engine.getInstance().init(gameObject.GetComponent<UnityContext>(),Config.GameID,
            Config.AppKey);


        appendLog("init+login....");
    }

    private void appendLog(string msg,params object[] msg1){
        Text.AddFirst(string.Format(msg,msg1));
        if(Text.Count>MaxLogLineCount){
            Text.RemoveLast();
        }
        string temp = "";
        foreach(var item in Text){
            temp += $"[{DateTime.Now}]: {item} \n";
        }
        GameObject.Find("Log").GetComponent<Text>().text = temp;
    }

    public int MaxLogLineCount = 20;

    public void send(string msg){ Engine.getInstance().sendEvent(msg);appendLog("send->"+msg); }
    public void stopMatch(){ Engine.getInstance().unInit(); }

}