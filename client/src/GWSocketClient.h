#ifndef GWSOCKETCLIENT_H_
#define GWSOCKETCLIENT_H_
#include <cstdlib>
#include <iostream>
#include <queue>
using namespace std;

#include "utils.h"
#include "common.h"
#include "SocketBase.h"
#include "SocketHandler.h"
#include "SocketChannel.h"
#include "SocketClient.h"
class GWSocketClient : public SocketClient{
public:
	GWSocketClient(SocketHandler *pSocketHandler,const char* host,int port,int recvTimeout);
	virtual ~GWSocketClient();
	void login(int userID, const char* token, void* client);
	virtual void heartbeat();
	int sendMsgToAll(char* buf, int size, void* client);
	int handleGWPacket(Packet* packet);
};

#endif /* GWSOCKETCLIENT_H_ */
