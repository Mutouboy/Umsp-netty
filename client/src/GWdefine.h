/*
 * GWdefine.h
 *
 *  Created on: 2016-5-7
 *      Author: geliang
 */

#ifndef GWDEFINE_H_
#define GWDEFINE_H_
#include "SocketBase.h"
extern "C" {
#define CMD_MSG_ID_FROM_TO_STRING            8001    //with fromID&toID
#define CMD_MSG_ID_FROM_TO_BYTES             8002    //with fromID&toID
#define CMD_MSG_JSON                         8003    //without fromID&toID

#define CMD_SendToAll 112
#define CMD_SingleMsg 110
#define CMD_MSG_SEND_GROUP      111

#define CMD_LOGIN               2506

#define CMD_GROUP_LIST          200 
#define CMD_GROUP_LIST_OK       201 
#define CMD_GROUP_JOIN          202 
#define CMD_GROUP_JOIN_OK       203 
#define CMD_GROUP_LEAVE         204 
#define CMD_GROUP_LEAVE_OK      205 
#define CMD_GROUP_USER          205 
#define CMD_GROUP_USERSTATE     206 
#define CMD_GROUP_USER_LEAVE    207 
#define CMD_GROUP_USER_JOIN     208 
#pragma pack(1)
typedef struct GWHeader {
	int size; 
	int version; 
	int seq; 
	int fromID; 
	int toID; 
	int cmd;
    GWHeader() {
        memset(this, 0, sizeof(GWHeader));
    }
} GWHeader;
typedef struct GWNullMsg {
	GWHeader header;
} GWNullMsg;
typedef struct GWMsg {
	GWHeader header;
	//	char msg[0];
} GWMsg;
typedef struct GWLogin {
	GWHeader header;
	int userid;
	char token[32];
} GWLogin;
#pragma pack()
EXPORT void login(int userID, const char* token, void* client);
EXPORT int sendMsgToAll(char* buf, int size, void* client);
EXPORT void* createTcpSocketClient(char* host, int hostSize, int port, int recvTimeout,int socketTag);
EXPORT void startLoop(void* loop,handlePacket handler,handleSocketClosed handler1);
/**
 * 返回 tag值 非0则表示该tag值下的socket已经关闭
 */
EXPORT int tryRecvPacket(handlePacket handler);
EXPORT void destorySocketClient(void * client);

/**
 * JSON
 * {"action":3,"msg":"value1","data":[{"key1":"value1"},{"key1":"value1"},{"key1":"value1"}],"bytes":"WFhYU1dYWFhY"}
 * bytes is hex encode
 */
EXPORT int sendJson(char* jsonString, int jsonStringSize, void* client);


EXPORT int tryRecvJsonPacket(handleJson handler);

EXPORT int initMethodBirdge();

}
#endif /* GWDEFINE_H_ */
