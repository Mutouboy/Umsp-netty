/*
 * SocketBace.h
 *
 *  Created on: 2016-5-7
 *      Author: geliang
 */

#ifndef SOCKETBACE_H_
#define SOCKETBACE_H_

extern "C" {
	typedef int (*handlePacket)(Packet* packet);
	typedef int (*handleSocketClosed)(int tag);
	/**
	 * Packet.data 为 二进制JSON数据<br>
	 * 固定格式如:{"action":3,"msg":"value1","data":[{"key1":"value1"},{"key1":"value1"},{"key1":"value1"}],"bytes":"WFhYU1dYWFhY"}
	 */
	typedef  handlePacket handleJson ;
#pragma pack()
}
#define ERR_TIMEOUT_SOCKET_CONNECT 1000
#define ERR_BUF_RECV 1001

#define ERR_OOM -1999
#define ERR_ILL_PARMS -2000
#define ERR_BUF_TOO_SMALL -2001
#define ERR_DIS_CONNECT_SOCKET -2002
#define ERR_OUT_BUF_SIZE -2003
#endif /* SOCKETBACE_H_ */
