﻿
using System;
using System.Collections;

namespace Umsp
{
    public class ConcurrentQueue<T>
    {
        private readonly Queue _queue = new Queue();
        private readonly object _syncObject = new object();
        public void Enqueue(T item)
        {
            lock (_syncObject)
            {
                _queue.Enqueue(item);
            }
        }
        public bool TryDequeue(out T item)
        {
            lock (_syncObject)
            {
                if (_queue.Count > 0)
                {
                    item = (T)_queue.Dequeue();
                    return true;
                }
                item = default(T);
                return false;
            }
        }
        public int Count
        {
            get
            {
                lock (_syncObject)
                {
                    return _queue.Count;
                }
            }
        }
    }
}