using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using System;
using AOT;
using System.Text;
using UnityEngine.UI;
using Umsp;
using JsonCsharp;
using System.Collections.Generic;
using Assets;
//class Debug {
//    public static void Log(String log) {

//    }
//    public static void LogWarning(String log) {

//    }
//    public static void LogWarning(String log, UnityEngine.Object obj) {

//    }
//    public static void LogWarning(String log, System.Object obj) {

//    }
//    public static void LogError(String log) {

//    }
//    public static void LogError(String log, UnityEngine.Object obj) {

//    }
//    public static void LogError(String log, System.Object obj) {

//    }
//    public static void DrawLine(Vector3 v1, Vector3 v2, Color color) {

//    }
//}
public class CallNativeCode : MonoBehaviour {
    
    public static CallNativeCode instance;
    private static volatile bool isStop = true;
    public volatile bool isLoopSyncTime = false;
    public const String Host = "192.168.8.155";
    public const int Port = 12345;
    public const int TimeOut = 100;



    public void run() {
       
        isStop = false;
        Debug.Log("========Thread start: istop =========" + isStop);
        int tag = new System.Random().Next(100000);
        System.IntPtr client = Assets.GWApi.NativeMethods.createTcpSocketClient(Encoding.UTF8.GetBytes(Host), Host.Length, Port, TimeOut, tag);
        //System.IntPtr client = Assets.GWApi.NativeMethods.createTcpSocketClient(Host, Host.Length, Port, TimeOut, 1);
        Debug.Log("create client:" + client);
        int userID = new System.Random().Next(100000);
        Loom.QueueOnMainThread(() => {
            showMessage("userID " + userID + " login" + "> " + Host + ":" + Port);
        });
        Assets.GWApi.NativeMethods.startLoop(client, handleJsonImp, handleScoketCloseImp);
        Assets.GWApi.NativeMethods.login(userID, "xxxx unity xxxx", client);
        Debug.Log("========login =========");
        DateTime lastTime = System.DateTime.Now;
        int loopCount = 0;
        while(!isStop) {
            loopCount++;
            if(isLoopSyncTime && loopCount %
                3 == 0) {
                sendDate();
            }
            if(msgQueue != null && msgQueue.Count > 0) {
                String data = "";
                while(msgQueue.TryDequeue(out data)) {
                    Debug.Log("sendMsgToAll: " + data);
                    //byte[] DataArray = Encoding.UTF8.GetBytes(data);
                    //Assets.GWApi.NativeMethods.sendMsgToAll(DataArray, DataArray.Length, client);
                    GWMsg msg = new GWMsg();
                    msg.bytes = Hex.bytesToString(Encoding.Default.GetBytes(data));
                    byte[] payload = Encoding.UTF8.GetBytes(Json<GWMsg>.toString(msg));
                    GWApi.NativeMethods.sendJson(payload, payload.Length, client);
                }
            }
            Thread.Sleep(16);

            int closedTag = Assets.GWApi.NativeMethods.tryRecvJsonPacket(handleJsonImp);
            //int closedTag = Assets.GWApi.NativeMethods.tryRecvPacket(handlePacketImp);

            if(closedTag == tag) {
                Debug.Log("socket: "+ closedTag+" had closed ");
                break;
           }
        }
        Debug.Log("xxxx  timeout or isstop xxxx");
        Assets.GWApi.NativeMethods.destorySocketClient(client);
        isStop =true;
        Debug.Log("========Thread: Stop =========");
    }

    public static double ExecDateDiff(DateTime dateBegin, DateTime dateEnd)
    {
        TimeSpan ts1 = new TimeSpan(dateBegin.Ticks);
        TimeSpan ts2 = new TimeSpan(dateEnd.Ticks);
        TimeSpan ts3 = ts1.Subtract(ts2).Duration();
        return ts3.TotalMilliseconds;
    }
    void Awake () {
        
        //获取需要监听的按钮对象设置这个按钮的监听，指向本类的ButtonClick方法中。NGUI
        UIEventListener.Get(GameObject.Find("BtnLogin")).onClick = ButtonClick;
        UIEventListener.Get(GameObject.Find("BtnLogout")).onClick = ButtonClick;
        UIEventListener.Get(GameObject.Find("BtnReady")).onClick = ButtonClick;
        UIEventListener.Get(GameObject.Find("BtnStart")).onClick = ButtonClick;
        UIEventListener.Get(GameObject.Find("BtnSend")).onClick = ButtonClick;
        UIEventListener.Get(GameObject.Find("BtnPay")).onClick = ButtonClick;
        UIEventListener.Get(GameObject.Find("BtnOver")).onClick = ButtonClick;

        instance = this;
        msgQueue = new ConcurrentQueue<String>();
        Loom.QueueOnMainThread(() => { }); ;
        isStop = true;

        testJson();
        this.StartCoroutine(this.testHTTP()); 
    }

    //计算按钮的点击事件
    void ButtonClick(GameObject button)
    {
        Debug.Log("GameObject " + button.name);
        switch (button.name)
        {
            case "BtnSend":
                sendDate();
                break;
            case "BtnRoomIn":
                break;

                
            case "BtnPay":
                syncTime();
                break;
            case "BtnLogin":
                if(isStop) {
                    Thread myThread = new Thread(run);
                    myThread.Start();
                } else {
                    showMessage("[FF0000] running...  stop please  [-]");
                }
                break;
            case "BtnLogout":
                isStop = true;
                showMessage("[FF0000]  stop   [-]");
                break;
            default:
                break;
        }

    }
    void OnDestroy()
    {
        isStop = true;
        isLoopSyncTime = false;
        Debug.Log("OnDestroy");
    }
    void Reset() {
        Debug.Log("Reset");
    }
    void OnApplicationQuit()
    {
        Debug.Log("OnApplicationQuit");
    }
    [AOT.MonoPInvokeCallback(typeof(Assets.GWApi.handlePacket))]
    private static int handlePacketImp(System.IntPtr packet) {
        var p = (Assets.GWApi.Packet)Marshal.PtrToStructure(packet, typeof(Assets.GWApi.Packet));
        if(p.size > 24) {
            byte[] dest = new byte[p.size - 24];
            Buffer.BlockCopy(p.data, 24, dest, 0, p.size - 24);
            System.String msg = Encoding.UTF8.GetString(dest);
            Debug.Log("handlePacketImp:" + msg + "data size:" + p.size);
            Loom.QueueOnMainThread(() => {
                if(CallNativeCode.instance != null) {
                    CallNativeCode.instance.SendMessage("showMessage", msg);
                } else {
                    Debug.Log("CallNativeCode.instance is null");
                }
            });
        }
        return 0;
    }
    private static int handleJsonImp(System.IntPtr packet) {
        var p = (Assets.GWApi.Packet)Marshal.PtrToStructure(packet, typeof(Assets.GWApi.Packet));
        if(p.size > 24) {
            byte[] dest = new byte[p.size];
            Buffer.BlockCopy(p.data, 0, dest, 0, p.size );
            System.String msg = Encoding.UTF8.GetString(dest);
            GWMsg gw = Json<GWMsg>.toObject(msg);
            if(gw.bytes!=null) {
                byte[] outArray = new byte[gw.bytes.Length/2];
                Hex.stringToBytes(gw.bytes, outArray);
                Debug.Log("handleJsonImp:" + msg + "data size:" + p.size+"\n"+ Encoding.Default.GetString(outArray));
                Loom.QueueOnMainThread(() => {
                if(CallNativeCode.instance != null) {
                    CallNativeCode.instance.SendMessage("showMessage", msg);
                } else {
                    Debug.Log("CallNativeCode.instance is null");
                }
            });
            }
        }
        return 0;
    }
    private static int handleScoketCloseImp(int tag) {
        Debug.Log("XXX  socket is close XXX");
        isStop = true;
        return 0;
    }
    public void showMessage(String msg)
    {
        //Debug.Log("showMessage:" + msg);
        //if (messageBuilder.Length > 512) { messageBuilder.Remove(0, messageBuilder.Length - 1); }
        //messageBuilder.AppendLine(msg);
        UILabel messageBox = GameObject.Find("MessageBox").GetComponent<UILabel>();
        messageBox.text = msg;
        //messageBox.text = messageBuilder.ToString();

    }
    ConcurrentQueue<String> msgQueue;
    public void sendDate() {
        var currentTime = System.DateTime.Now.ToString()+":"+ System.DateTime.Now.Millisecond + "sxxx 2 chinese";
        Debug.Log("sendDate : getCurrentTime:" + currentTime);
        msgQueue.Enqueue(currentTime);
    }
    void OnGUI() {
        float x = 3;
        float y = 10;
        GUI.Label(new Rect(15, 125, 450, 100), "adding " + x + " and " + y + " in native code equals ");
    }

    public void syncTime()
    {
        isLoopSyncTime = !isLoopSyncTime;
    }

    public void exit()
    {

#if UNITY_EDITOR
        showMessage("Can`t exit in Editor");
#elif UNITY_IPHONE || UNITY_ANDROID || UNITY_STANDALONE
             Application.Quit();
#endif
    }
    public class GWMsg {
        public int action = 112;
        public String msg = "";
        public String bytes= "";
        public override string ToString() {
            return "action:"+action+" msg:"+msg+" bytes:"+bytes;
        }
    }
    public class TA {
        public int c = 100;
        public uint d = 1000;
        public long e = -10000000;
        public ulong f = 30000000000;
        public bool g = true;
        public double h = 0.0f;
        public double i = 0.000000001f;
    }
    class A {
        [JsonName("1")]
        public int a = 1;

        [JsonName("2")]
        public int B = 3;

        [JsonIgnore]
        public int[] cs = new int[3];
        public List<TA> tttt = new List<TA>();

        public uint[] cs1 = null;
        [JsonIgnore]
        public List<TA> tttt1 = null;

        public DateTime tt = DateTime.Now;
        public string tt2 = null;

        public int TQ;

        private int dtq = 1;
        [JsonIgnore]
        public const int dc = 12;

        [JsonName("tq1")]
        public int TQ1 { get { return dtq; } set { dtq = value; } }

        public char Car = (char)11;
        public byte Bar = (byte)12;
        public char[] Carr = new char[3];
        public byte[] Barr = new byte[3];

        public int q1 = 0;
        public int q2 = 0;

        public A() {
            Debug.Log("A init[]" );
            TA ta = new TA();
            tttt.Add(ta);
        }
    };
    public IEnumerator testHTTP() {
        WWW www = new WWW("http://192.168.8.4:8085/mddoc/api/index"); 

        yield return www;
        if(!string.IsNullOrEmpty(www.error)) {
            showMessage("[FF0000] http err [-]" + www.error);
        }
        if(www.isDone) {
            showMessage("[FF0000]  http done  [-]"+ www.text.Substring(0,100));
        }
    }
    public void sendJson(String data) {

    }
    public  void testJson() {
        A a = new A();
        a.a = 11;
        a.B = 22;
        String json = Json<A>.toString(a);
        showMessage( json+ "\n"+Json <A> .toString(Json<A>.toObject(json)));
        Debug.Log("char to byte" + (byte)'h');
        Debug.Log(Encoding.Default.EncodingName + "\n");
        GWMsg msg = new GWMsg();
        const string hellword = "你好abc1111111111";
        msg.bytes  = Base64.encodeToString(Encoding.Default.GetBytes(hellword), 0);
        Debug.Log("encode=>"+ msg.bytes);
        var decoded = Encoding.Default.GetString(Base64.decode(msg.bytes, 0));
        showMessage("decode=>"+ decoded + "");
        string base64 = "eyJhY3Rpb24iOjMsIm1zZyI6InZhbHVlMSIsImRhdGEiOlt7ImtleTEiOiJ2YWx1ZTEifSx7ImtleTEiOiJ2YWx1ZTEifSx7ImtleTEiOiJ2YWx1ZTEifV19";
        //byte[] b = System.Text.Encoding.;
        ////转成 Base64 形式的 System.String
        //a = Convert.ToBase64String(b);
        //Response.Write(a);
        //var decode = Base64.decodingforstring(msg.bytes);
        //Debug.Log("" + decode);
        //showMessage("base64 to String:\n" + decode);
        Debug.Log(Encoding.Default.GetString(Base64.decode("5L2g5aW9YWJj",0)));


        byte[] src = { 0x1, 0x22, (byte)0xFF, 0x4f };
        String bytesToString = Hex.bytesToString(src);
        Debug.Log(bytesToString);
        byte[] outArray = new byte[bytesToString.Length / 2];
        Hex.stringToBytes(bytesToString, outArray);
        for(int i = 0; i < outArray.Length; i++) {
            Debug.Log(outArray[i] & 0xFF);
        }
        
    }
    public static String ToUTF8(String src) {
        return Encoding.UTF8.GetString(Encoding.Default.GetBytes(src));
    }
    public static String UTF8ToDefault(String src) {
        return Encoding.Default.GetString(Encoding.UTF8.GetBytes(src));
    }
}
